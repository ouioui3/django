from django.db import models

class Bibliotheque(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name
    
class Tag(models.Model):
    name = models.CharField(max_length=250)

class Jeux(models.Model):
    picture = models.CharField(max_length=250)
    name = models.CharField(max_length=250)
    bibliotheque = models.ForeignKey(Bibliotheque, on_delete=models.CASCADE)
    tags = models.ManyToManyField(Tag)

