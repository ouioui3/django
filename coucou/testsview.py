from django.test import TestCase
from django.urls import reverse

class IndexViewTest(TestCase):
    def test_index_view(self):
        response = self.client.get(reverse('index'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Coucou')

class BibliothequeViewTest(TestCase):
    def test_bibliotheque_view(self):
        response = self.client.get(reverse('bibliotheque'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'bibliotheque.html')

class BibliothequeGamesViewTest(TestCase):
    def test_bibliotheque_games_view(self):
        biblio_id = 1 
        response = self.client.get(reverse('bibliotheque_games', args=[biblio_id]))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'bibliogames.html')
        
