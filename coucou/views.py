from django.shortcuts import render
from django.http import HttpResponse
from django.http import HttpResponseServerError
from django.views.generic import ListView
from .models import Bibliotheque, Tag, Jeux
from django.template import loader


def index(request):
    return HttpResponse('Coucou')

def bibliotheque(request):
    bibliotheque = Bibliotheque.objects.all()
    template = loader.get_template("bibliotheque.html")
    context = {
        "biblis": bibliotheque,
    }
    return HttpResponse(template.render(context, request))

def bibliotheque_games(request, listid):
    bibliotheque = Bibliotheque.objects.filter(id = listid)
    games = Jeux.objects.filter(bibliotheque = listid)
    template = loader.get_template("bibliogames.html")
    context = {
        'biblio':None,
        'games': None,
    }
    if len(bibliotheque) == 1:
        context['biblio']= bibliotheque[0]
        context['games']=games
    return HttpResponse(template.render(context, request))




