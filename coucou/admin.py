from django.contrib import admin
from .models import Bibliotheque
from .models import Tag
from .models import Jeux

# Register your models here.

admin.site.register(Bibliotheque)
admin.site.register(Tag)
admin.site.register(Jeux)