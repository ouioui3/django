from selenium import webdriver
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from parameterized import parameterized
from selenium.webdriver.common.by import By

class IndexViewSeleniumTest(StaticLiveServerTestCase):

    fixtures = ['init.json']

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.selenium = webdriver.Chrome() 
        cls.selenium.implicitly_wait(100)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()

    def test_index_view(self):
        self.selenium.get(self.live_server_url)
        self.assertIn("Coucou", self.selenium.page_source)

    def test_bibliotheque_view(self):
        self.selenium.get(self.live_server_url + '/bibliotheque')
        self.assertIn("Bibliotheques", self.selenium.page_source)

    @parameterized.expand([
        (1,"Installed"), #biblio_id, biblio_game
        (3, "Finished"), 
        (4, "Started")
        ])
    
    def test_bibliotheque_games_view(self, biblio_id, biblio_game ):
        self.selenium.get(self.live_server_url + f'/bibliotheque/{biblio_id}')
        self.assertIn(biblio_game, self.selenium.page_source) 

    @parameterized.expand([
        (1,0, 2), #biblio_id,  biblio_pos, expected_nb_items
        (3,1, 2), 
        (4,2, 2)
        ])
    
    def test_selenium_bibliotheque_games(self, biblio_id, biblio_pos, expected_nb_items ):
        self.selenium.get(f"{self.live_server_url}/bibliotheque")
        links = self.selenium.find_elements(By.TAG_NAME, 'a')
        links[biblio_pos].click()
        shoplistitems = self.selenium.find_elements(By.TAG_NAME, 'li')
        self.assertEqual(expected_nb_items, len(shoplistitems))