from django.test import TestCase
from .models import Jeux, Bibliotheque, Tag
from parameterized import parameterized
from django.db.utils import DataError

class JeuxTest(TestCase):
    fixtures = ['init']

    @parameterized.expand([
        ("test", "jeux1", 2, 1), 
        ("test","jeux2", 0, 0)])

    def test_add_game_without_tags(self, picture, name, tag_id, nb_expected):
        bibliotheque = Bibliotheque.objects.create(name="Ma Bibliothèque")

        jeu_sans_tags = Jeux.objects.create(
            picture=picture,
            name=name,
            bibliotheque=bibliotheque,
        )
        name_tag = Tag.objects.filter(id=tag_id)
        if len(name_tag) > 0:
            e=name_tag[0]
            jeu_sans_tags.tags.add(e)

        self.assertEqual(jeu_sans_tags.tags.count(), nb_expected)

    
    def test_create_game_with_tags(self):
        bibliotheque = Bibliotheque.objects.create(name="Ma Bibliothèque")
        tag1 = Tag.objects.create(name="Tag1")
        tag2 = Tag.objects.create(name="Tag2")
    
        jeu_avec_tags = Jeux.objects.create(
            picture="test_picture",
            name="Test Game",
            bibliotheque=bibliotheque,
        )
        jeu_avec_tags.tags.add(tag1, tag2)

        self.assertEqual(jeu_avec_tags.tags.count(), 2)

    def test_create_tag(self):
        tag = Tag.objects.create(name="Test Tag")
        self.assertEqual(tag.name, "Test Tag")

    def test_delete_game(self):
        bibliotheque = Bibliotheque.objects.create(name="Ma Bibliothèque")
        game_to_delete = Jeux.objects.create(
            picture="test_picture",
            name="Game to Delete",
            bibliotheque=bibliotheque,
        )
        game_id = game_to_delete.id
        game_to_delete.delete()

        with self.assertRaises(Jeux.DoesNotExist):
            Jeux.objects.get(pk=game_id)
    